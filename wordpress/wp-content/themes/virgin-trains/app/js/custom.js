var virgin = {

	init: function () {
		this.bindings();
		this.createMap();
	},

	bindings: function () {

		jQuery('.menu-toggle').click(virgin.showMenu);
		jQuery('#booking-status li a').click(virgin.bookingStage);
		jQuery('.results-container .result').click(virgin.selectTicket);
		jQuery('#results .show-all-fares').click(virgin.showAllResults);

		jQuery('#booking-form').on('submit', function(e) { 
			e.preventDefault();
			virgin.fakeBooking();
		});

	},
	fakeBooking: function (e) {

		var origin_station = jQuery("#booking-form input[name='origin_station']").val();
		//Override with Kings Cross as per design
		origin_station = 'London<br>Kings Cross<br>KGX';

		var $bookingSectionConatiner = jQuery('.booking-container'),
			$bookingSection = jQuery('.booking-stage[data-booking-stage=your-tickets]');
			$bookingSectionPosition = $bookingSection.index() * -1;

		$bookingSectionConatiner.css("transform","translate3d(0px, " + $bookingSectionPosition + "00%, 0px)");

		jQuery('#booking-status a.active').removeClass('active');
		jQuery('#booking-status a[data-booking-stage-link=your-tickets]').addClass('active');

		//Fake Booking Process
		jQuery('#booking-status a[data-booking-stage-link=starting-from]').addClass('has-value');
		jQuery('#booking-status a[data-booking-stage-link=starting-from] span.value').html(origin_station);

		jQuery('#booking-status a[data-booking-stage-link=travelling-to]').addClass('has-value');
		jQuery('#booking-status a[data-booking-stage-link=travelling-to] span.value').html('Wakefield Westgate<br>WKF');

		jQuery('#booking-status a[data-booking-stage-link=travelling-when]').addClass('has-value');
		jQuery('#booking-status a[data-booking-stage-link=travelling-when] span.value').html('Oct.09<br>12:30am');

		jQuery('#booking-status a[data-booking-stage-link=who]').addClass('has-value');
		jQuery('#booking-status a[data-booking-stage-link=who] span.value').html('2 Adults');

		jQuery('#booking-status a[data-booking-stage-link=extras]').addClass('has-value');
		jQuery('#booking-status a[data-booking-stage-link=extras] span.value').html('None');

		jQuery('#booking-status a[data-booking-stage-link=your-tickets]').removeAttr('disabled');

		jQuery('#booking-status').addClass('docked');
	},
	showMenu: function (e) {

		e.preventDefault();

		jQuery(this).toggleClass('active');
		jQuery('.main-menu').fadeToggle();
		
	},
	bookingStage: function (e) {
		
		var $this = jQuery(this),
			$bookingSectionConatiner = jQuery('.booking-container'),
			$bookingSectionLink = $this.data('booking-stage-link'),
			$bookingSection = jQuery('.booking-stage[data-booking-stage=' + $bookingSectionLink + ']');
			$bookingSectionPosition = $bookingSection.index() * -1;

		if( $this.attr('disabled') != 'disabled'  ) {
			jQuery('#booking-status a.active').removeClass('active');
			$this.addClass('active');
	
			$bookingSectionConatiner.css("transform","translate3d(0px, " + $bookingSectionPosition + "00%, 0px)");

			if( $bookingSectionLink == 'your-tickets' ) {
				jQuery('#booking-status').addClass('docked');
			} else {
				jQuery('#booking-status.docked').removeClass('docked');
			}
		}

	},
	createMap: function (e) {

		if( jQuery(window).width() > 640 ) {
			var zoomLevel = 8;
		} else {
			var zoomLevel = 6;
		}

		//Create Map
		var map = new google.maps.Map(document.getElementById('map'), {
			zoom: zoomLevel,
			center: {lat: 52.651283, lng: -0.480216},
			disableDefaultUI: true
		});

		//Style Map
		var virginTrainsStyle = new google.maps.StyledMapType(
			[
				{
					"featureType": "all",
					"elementType": "labels.text.fill",
					"stylers": [
						{
							"saturation": 36
						},
						{
							"color": "#000000"
						},
						{
							"lightness": 40
						}
					]
				},
				{
					"featureType": "all",
					"elementType": "labels.text.stroke",
					"stylers": [
						{
							"visibility": "on"
						},
						{
							"color": "#000000"
						},
						{
							"lightness": 16
						}
					]
				},
				{
					"featureType": "all",
					"elementType": "labels.icon",
					"stylers": [
						{
							"visibility": "off"
						}
					]
				},
				{
					"featureType": "administrative",
					"elementType": "geometry.fill",
					"stylers": [
						{
							"color": "#000000"
						},
						{
							"lightness": 20
						}
					]
				},
				{
					"featureType": "administrative",
					"elementType": "geometry.stroke",
					"stylers": [
						{
							"color": "#000000"
						},
						{
							"lightness": 17
						},
						{
							"weight": 1.2
						}
					]
				},
				{
					"featureType": "landscape",
					"elementType": "geometry",
					"stylers": [
						{
							"color": "#000000"
						},
						{
							"lightness": 20
						}
					]
				},
				{
					"featureType": "poi",
					"elementType": "geometry",
					"stylers": [
						{
							"color": "#000000"
						},
						{
							"lightness": 21
						}
					]
				},
				{
					"featureType": "road.highway",
					"elementType": "geometry.fill",
					"stylers": [
						{
							"color": "#000000"
						},
						{
							"lightness": 17
						}
					]
				},
				{
					"featureType": "road.highway",
					"elementType": "geometry.stroke",
					"stylers": [
						{
							"color": "#000000"
						},
						{
							"lightness": 29
						},
						{
							"weight": 0.2
						}
					]
				},
				{
					"featureType": "road.arterial",
					"elementType": "geometry",
					"stylers": [
						{
							"color": "#000000"
						},
						{
							"lightness": 18
						}
					]
				},
				{
					"featureType": "road.local",
					"elementType": "geometry",
					"stylers": [
						{
							"color": "#000000"
						},
						{
							"lightness": 16
						}
					]
				},
				{
					"featureType": "transit",
					"elementType": "geometry",
					"stylers": [
						{
							"color": "#000000"
						},
						{
							"lightness": 19
						}
					]
				},
				{
					"featureType": "water",
					"elementType": "geometry",
					"stylers": [
						{
							"color": "#000000"
						},
						{
							"lightness": 17
						}
					]
				}
			],
			{name: 'Virgin Trains'});
	
			map.mapTypes.set('virgin_trains', virginTrainsStyle);
			map.setMapTypeId('virgin_trains');
		
		//Add Markers
		var trainJourneyCoords = [
			{lat: 53.682937, lng: -1.505787},  //Wakefield
			{lat: 53.522164, lng: -1.139288},  //Doncaster
			{lat: 52.906607, lng: -0.642903},  //Grantham
			{lat: 51.901660, lng: -0.207040},  //Stevenage
			{lat: 51.531651, lng: -0.124412}   //Kings Cross
		];

		var wakefieldMarkerImage = {
			url: '/wp-content/themes/virgin-trains/app/img/icons/location-pin.png',
			size: new google.maps.Size(41, 59),
			origin: new google.maps.Point(0, 0),
			anchor: new google.maps.Point(10, 15),
			scaledSize: new google.maps.Size(20, 30)
		  };

        var wakefieldMarker = new google.maps.Marker({
          position: {lat: trainJourneyCoords[0].lat, lng: trainJourneyCoords[0].lng},
          map: map,
		  icon: wakefieldMarkerImage
		});

		var doncasterMarkerImage = {
			url: '/wp-content/themes/virgin-trains/app/img/icons/stop-1.png',
			size: new google.maps.Size(72, 90),
			origin: new google.maps.Point(0, 0),
			anchor: new google.maps.Point(18, 22),
			scaledSize: new google.maps.Size(36, 45)
		  };

        var doncasterMarker = new google.maps.Marker({
          position: {lat: trainJourneyCoords[1].lat, lng: trainJourneyCoords[1].lng},
          map: map,
		  icon: doncasterMarkerImage
		});

		var granthamMarkerImage = {
			url: '/wp-content/themes/virgin-trains/app/img/icons/stop-2.png',
			size: new google.maps.Size(72, 90),
			origin: new google.maps.Point(0, 0),
			anchor: new google.maps.Point(18, 22),
			scaledSize: new google.maps.Size(36, 45)
		  };

        var granthamMarker = new google.maps.Marker({
          position: {lat: trainJourneyCoords[2].lat, lng: trainJourneyCoords[2].lng},
          map: map,
		  icon: granthamMarkerImage
		});

		var stevenageMarkerImage = {
			url: '/wp-content/themes/virgin-trains/app/img/icons/stop-3.png',
			size: new google.maps.Size(72, 90),
			origin: new google.maps.Point(0, 0),
			anchor: new google.maps.Point(18, 22),
			scaledSize: new google.maps.Size(36, 45)
		  };

        var stevenageMarker = new google.maps.Marker({
          position: {lat: trainJourneyCoords[3].lat, lng: trainJourneyCoords[3].lng},
          map: map,
		  icon: stevenageMarkerImage
		});

		var kingsCrossMarkerImage = {
			url: '/wp-content/themes/virgin-trains/app/img/icons/destination-pin.png',
			size: new google.maps.Size(41, 59),
			origin: new google.maps.Point(0, 0),
			anchor: new google.maps.Point(10, 15),
			scaledSize: new google.maps.Size(20, 30)
		  };

        var kingsCrossMarker = new google.maps.Marker({
          position: {lat: trainJourneyCoords[4].lat, lng: trainJourneyCoords[4].lng},
          map: map,
		  icon: kingsCrossMarkerImage
		});

		//Draw Journey Liune
		var trainJourney = new google.maps.Polyline({
			path: trainJourneyCoords,
			geodesic: true,
			strokeColor: '#FFFFFF',
			strokeOpacity: 1.0,
			strokeWeight: 3
		});
	
		trainJourney.setMap(map);
		
		
	},
	selectTicket: function (e) {
		
		var $this = jQuery(this);

		jQuery('.results-container .result.selected').removeClass('selected');
		$this.addClass('selected');

	},
	showAllResults: function (e) {

		var $showAllButton = jQuery(this).find('span');

		if( jQuery('#results').hasClass('show-all') ) {
			$showAllButton.html('Show All Fares').parent().removeClass('hide');
		} else {
			$showAllButton.html('Hide All Fares').parent().addClass('hide');
		}

		jQuery('#results').toggleClass('show-all');

	},
};

jQuery(window).load(function () {
	virgin.init();
});