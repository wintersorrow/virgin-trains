const pkg = require("./package.json");
const gulp = require("gulp");
const cleanCSS = require('gulp-clean-css');

const $ = require("gulp-load-plugins")({
    pattern: ["*"],
    scope: ["devDependencies"]
});

var customScripts = 'src/js/custom/*.js',
    customScriptsDest = 'app/js/';

gulp.task('custom-scripts', function() {
    return gulp.src(customScripts)
        .pipe($.concat('custom.js'))
        .pipe(gulp.dest(customScriptsDest))
        .pipe($.rename('custom.min.js'))
        .pipe($.uglify())
        .pipe(gulp.dest(customScriptsDest));
});

gulp.task('sass', function(){
    return gulp.src('src/scss/styles.scss')
        .pipe($.sass())
        .pipe($.autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('app/css'))
        .pipe($.rename('styles.min.css'))
        .pipe(cleanCSS())
        .pipe($.sourcemaps.write())
        .pipe(gulp.dest('app/css'))
});

gulp.task('imagemin', function(){
    return gulp.src(['src/img/*', 'src/img/**/*'])
    .pipe($.imagemin())
    .pipe(gulp.dest('app/img'))
});

gulp.task('default', ['custom-scripts', 'sass', 'imagemin'], function (){

    function logFileChange(event) {
        var fileName = require('path').relative(__dirname, event.path);
        console.log('[' + 'WATCH'.green + '] ' + fileName.magenta + ' was ' + event.type + ', running tasks...');
    }

    gulp.watch(['src/scss/**/*.scss'], ['sass'])
        .on('change', function(event) {
        logFileChange(event);
    });
});
