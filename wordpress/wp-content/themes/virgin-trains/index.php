<?php get_header(); ?>


<div id="sidebar">
    <a href="/" class="logo">
        <img src="<?php echo get_template_directory_uri(); ?>/app/img/virgin-logo.png" alt="Virgin Trains" width="140">
    </a>

    <div id="booking-status">
        <ul>
            <li>
                <a class="active" data-booking-stage-link="starting-from">
                    <span>From</span>
                    <span class="value"></span>
                    <i class="icon icon-from"></i>
                </a>
            </li>
            <li>
                <a data-booking-stage-link="travelling-to" disabled>
                    <span class="label">To</span>
                    <span class="value"></span>
                    <i class="icon icon-to"></i>
                </a>
            </li>
            <li>
                <a data-booking-stage-link="travelling-when" disabled>
                    <span class="label">When</span>
                    <span class="value"></span>
                    <i class="icon icon-when"></i>
                </a>
            </li>
            <li>
                <a data-booking-stage-link="who" disabled>
                    <span class="label">Who</span>
                    <span class="value"></span>
                    <i class="icon icon-who"></i>
                </a>
            </li>
            <li>
                <a data-booking-stage-link="extras" disabled>
                    <span class="label">Extras</span>
                    <span class="value"></span>
                    <i class="icon icon-extras"></i>
                </a>
            </li>
            <li>
                <a data-booking-stage-link="your-tickets" disabled>
                    <span class="label">Your Tickets</span>
                    <span class="value"></span>
                    <i class="icon icon-ticket"></i>
                </a>
            </li>
        </ul>
    </div>
</div>

<div class="booking-container">
    <section class="booking-stage starting-from" data-booking-stage="starting-from" style="background-image: url('<?php echo get_template_directory_uri(); ?>/app/img/backgrounds/booking/start-your-journey.jpg'); ">
        <div class="form-container">
            <p class="subtitle">START YOUR JOURNEY</p>
            <h1>Where would<br>you like to go?</h1>
            <form id="booking-form">
                <input autocomplete="off" name="origin_station" placeholder="Station or Location" title="Station or Location" type="text" value="" tabindex="0" required>
                <button class="button" tabindex="1"><i class="icon icon-arrow"></i></button>
            </form>
        </div>
    </section>
    
    <section class="booking-stage travelling-to" data-booking-stage="travelling-to" style="background-image: url('<?php echo get_template_directory_uri(); ?>/app/img/backgrounds/booking/placeholder.jpg'); ">
        <!-- To Do: Travelling To -->
    </section>

    <section class="booking-stage travelling-when" data-booking-stage="travelling-when" style="background-image: url('<?php echo get_template_directory_uri(); ?>/app/img/backgrounds/booking/placeholder.jpg'); ">
        <!-- To Do: When -->
    </section>

    <section class="booking-stage who" data-booking-stage="who" style="background-image: url('<?php echo get_template_directory_uri(); ?>/app/img/backgrounds/booking/placeholder.jpg'); ">
        <!-- To Do: Who -->
    </section>

    <section class="booking-stage extras" data-booking-stage="extras" style="background-image: url('<?php echo get_template_directory_uri(); ?>/app/img/backgrounds/booking/placeholder.jpg'); ">
        <!-- To Do: Extras -->
    </section>
    
    <section class="booking-stage your-tickets" data-booking-stage="your-tickets">
        <div id="map"></div>
        <div id="results">
            <a class="show-all-fares"><span>Show All Fares</span><i class="icon icon-arrow"></i></a>
            <div class="results-container">
                <?php for ($x = 0; $x <= 10; $x++) : ?>
                    <div class="result<?php if($x == 0) echo ' recommended selected'; ?>">
                        <div class="details">
                            <div class="detail times">13:58 - 16:00</div>
                            <div class="detail cost">£102.00</div>
                            <div class="detail duration">2:01hrs</div>
                            <a class="button">Buy</a>
                        </div>
                    </div>
                <?php endfor; ?>
            </div>
        </div>
    </section>
</div>

<?php get_footer(); ?>
