<?php

//////////////
//	External Files
//////////////

//////////////
//	Theme Support
//////////////

function load_header_scripts()
{
    if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {

        wp_register_script('jquery', get_template_directory_uri() . '/app/js/jquery.js', array(), filemtime(get_stylesheet_directory() . '/app/js/jquery.js'));
        wp_enqueue_script('jquery');

    	wp_register_script('conditionizr', get_template_directory_uri() . '/app/js/conditionizr-4.3.0.min.js', array(), '4.3.0'); // Conditionizr
        wp_enqueue_script('conditionizr');

        wp_register_script('modernizr', get_template_directory_uri() . '/app/js/modernizr-2.7.1.min.js', array(), '2.7.1'); // Modernizr
        wp_enqueue_script('modernizr');

        wp_register_script('custom-scripts', get_template_directory_uri() . '/app/js/custom.min.js', array(), filemtime(get_stylesheet_directory() . '/app/js/custom.min.js'));
        wp_enqueue_script('custom-scripts');

    }
}

function load_styles()
{
    wp_register_style('normalize', get_template_directory_uri() . '/app/css/normalize.min.css', array(), '1.0', 'all');
    wp_enqueue_style('normalize');

    wp_register_style('virgin-trains', get_template_directory_uri() . '/app/css/styles.min.css', array(), '1.0', 'all');
    wp_enqueue_style('virgin-trains'); 
}

function add_slug_to_body_class($classes)
{
    global $post;
    if (is_home()) {
        $key = array_search('blog', $classes);
        if ($key > -1) {
            unset($classes[$key]);
        }
    } elseif (is_page()) {
        $classes[] = sanitize_html_class($post->post_name);
    } elseif (is_singular()) {
        $classes[] = sanitize_html_class($post->post_name);
    }

    return $classes;
}

//////////////
//	Actions & Filters
//////////////

// Add Actions
add_action('init', 'load_header_scripts');
add_action('wp_print_scripts', 'load_conditional_scripts');
add_action('wp_enqueue_scripts', 'load_styles');

// Add Filters
add_filter('body_class', 'add_slug_to_body_class');
