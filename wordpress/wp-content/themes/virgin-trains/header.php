<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>

		<link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_template_directory_uri(); ?>/app/img/favicons/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_template_directory_uri(); ?>/app/img/favicons/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/app/img/favicons/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_template_directory_uri(); ?>/app/img/favicons/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/app/img/favicons/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_template_directory_uri(); ?>/app/img/favicons/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_template_directory_uri(); ?>/app/img/favicons/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_template_directory_uri(); ?>/app/img/favicons/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/app/img/favicons/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo get_template_directory_uri(); ?>/app/img/favicons/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/app/img/favicons/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="<?php echo get_template_directory_uri(); ?>/app/img/favicons/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/app/img/favicons/favicon-16x16.png">

		<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAs30WgthU4kpZtTRcq5qUs-yRf4gzVb5w" type="text/javascript"></script>

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="<?php bloginfo('description'); ?>">

		<?php wp_head(); ?>

	</head>
	<body <?php body_class(); ?>>

		<div class="wrapper"> 

			<header class="top-bar">
				<a href="/" class="mobile-logo">
					<img src="<?php echo get_template_directory_uri(); ?>/app/img/virgin-logo-white.png" alt="Virgin Trains" width="132">
				</a>
				<ul class="usp-block">
					<li>Unbeatable prices. Guaranteed</li>
					<li>No booking fees. No card feeds</li>
					<li>Cheap train tickets. Faster</li>
				</ul>
				<div class="mini-nav">
					<a class=""><i class="icon icon-search"></i></a>
					<a class="my-account"><i class="icon icon-account"></i></a>
				</div>
				<a class="menu-toggle" href="#">
					<div class="burger-box">
						<div class="burger-filling"></div>
					</div>
				</a>
				<div class="main-menu">
					<div class="main-menu-container">
						<ul>
							<li><a href="#">Menu Link 1</a></li>
							<li><a href="#">Menu Link 2</a></li>
							<li><a href="#">Menu Link 3</a></li>
							<li><a href="#">Menu Link 4</a></li>
							<li><a href="#">Menu Link 5</a></li>
						</ul>
					</div>
				</div>
			</header>
