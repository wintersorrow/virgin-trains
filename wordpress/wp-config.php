<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'virgin_trains');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Sll<<#eSW4:Fw3{([N/23n/5O+bw]?!L&w1|J|jEv8Op^)=7LRkST`t;^==E7_]9');
define('SECURE_AUTH_KEY',  ' @S&wJg?<B}SlPm$L+GN1gNOp}VQRmw<d.?`v-rJH5eHsBaCHx%nw<{^8wkZ!BWf');
define('LOGGED_IN_KEY',    '`o<zuU9+/|H53=C8V!q;y]PS_bej1^tJt{U#+vT@hT8YY>mSPB;uoqoZa(;aR(lI');
define('NONCE_KEY',        '{jyVlCX=^U,T[36T&0iv;)nrFXE:]vs[[!gJ]V;J2P3ra-=rh#</r^EMMrWVNl+4');
define('AUTH_SALT',        'n5I,DC1E(-X6kVSSo&R4Pl|wb]0->]Me9`C#pTVAC!SIJNNY!ZKE(n6.^Ohp>9+*');
define('SECURE_AUTH_SALT', 'cjZyq&-Q4tf$5.0AdN>B6$@AIZ}hxgJ?67M&-Y>d^Rvy,3 {l?vCm,/%.]<L=kSv');
define('LOGGED_IN_SALT',   '% 9`Mg#qA@Vc_W?^a: #902eO[qvKD8FCq.sUznu=k~lsEwgaccu3`R989Dph*>t');
define('NONCE_SALT',       '7c[|i s I]qr(!S0hS<b*43f]40H[Q_|12XrCZ|/+97nzeS2;.Y3B(RzT6S_,~eX');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
